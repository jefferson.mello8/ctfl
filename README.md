Olá pessoal,

Estou divulgando esses áudios para auxiliar quem estuda para a certificação CTFL
e gosta de escutar o material para melhor fixar o assunto.

Lembrando que não estou cobrando nada pela divulgação desse material, até mesmo
porque o Syllabus é grátis!

Espero que posso ajudar, já estão disponíveis os itens 1, 2, 3, 4 e 5 do Syllabus, 
estou gravando o item 6 e logo estará disponível.

Assim que finalizar a gravação e edição de todos os itens, vou começar a regravar
alguns áudios do início que ficaram com muitos ruídos no fundo.

PS.: Minha dicção não é a melhor do mundo, então recomendo que escutar em 1.5x ou 2x, espero poder ajudar :)

Qualquer dica ou melhoria envie para o e-mail lourenciniwilliam@hotmail.com ou
@wlourencini no telegram.

Att.